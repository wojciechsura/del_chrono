#include "Display.h"
#include "Keypad.h"
#include "Menu.h"
#include "EnableInterrupt.h"

// Global variables -----------------------------------------------------------

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
Display display(lcd, 16, 2);
Keypad keypad;

BaseMenuItem * mainItems[] = 
{
  new ActionMenuItem("\x01 Pomiar FPS", 1),
};

MainMenu mainMenu(display, mainItems, sizeof(mainItems) / sizeof(BaseMenuItem *));

// *** FPS measurement ***

unsigned long fpsStart;
unsigned long fpsEnd;
int fpsMode = 0;

// Global functions -----------------------------------------------------------

void detector1Up()
{
  fpsStart = micros();
  fpsMode = 1;

  disableInterrupt(A1);
  enableInterrupt(A2, detector2Up, RISING);  
}

void detector2Up()
{
  fpsEnd = micros();
  fpsMode = 2;
  disableInterrupt(A2);
}

void fpsDisplay(long ms)
{
  lcd.clear();
  
  lcd.setCursor(0, 0);
  lcd.print("ms: ");
  lcd.print(ms);
}

void fpsDisplayMissedError()
{
  lcd.clear();

  lcd.print(" *** Error *** ");
  lcd.setCursor(0, 1);
  lcd.print("Missed 2 sensor");
}

void fpsMeasurement()
{
  fpsMode = 0;
  enableInterrupt(A1, detector1Up, RISING);

  fpsDisplay(0);

  bool finish = false;
  while (!finish)
  {
    if (fpsMode == 1)
    {
      // Checking if particle missed second sensor
      
      unsigned long now = micros();
      if (now > fpsStart && now - fpsStart > 1000000)
      {
        // If second passed after first measurement, assume, that
        // particle missed second sensor
    
        // Stop waiting for second sensor
        disableInterrupt(A2);
        
        fpsDisplayMissedError();

        // Restart measurement
        fpsMode = 0;
        enableInterrupt(A1, detector1Up, RISING);
      }
    }    
    
    if (fpsMode == 2)
    {
      // Checking if measurement was made
      
      if (fpsEnd > fpsStart)
      {
        unsigned long timeDist = fpsEnd - fpsStart;
        fpsDisplay(timeDist);

        Serial.print("Measured time: ");
        Serial.print(timeDist);
        Serial.print("\n");
      }
      
      fpsMode = 0;
      enableInterrupt(A1, detector1Up, RISING);
    }
    else
    {
      // Waiting for keypress - exiting mode
      
      int key = keypad.ReadKey();
      if (key != KEY_NONE)
        finish = true;
    }
  }

  disableInterrupt(A1);
  disableInterrupt(A2);
}

void setup()
{
  Serial.begin(9600);
  while (!Serial);

  byte gun[8] = {
    B00000,
    B11000,
    B11000,
    B11000,
    B11110,
    B11111,
    B11011,
    B00000,
  };

  byte reverseGun[8] = {
    B00000,
    B00011,
    B00011,
    B00011,
    B01111,
    B11111,
    B11011,
    B00000,
  };

  byte fps[8] = {
    B01110,
    B10001,
    B10001,
    B01110,
    B00000,
    B10100,
    B10101,
    B10001
  };

  byte bullet[8] = {
    B11000,
    B11000,
    B00000,
    B10000,
    B10000,
    B00000,
    B01000,
    B01000
  };

  byte reverseBullet[8] = {
    B00011,
    B00011,
    B00000,
    B00001,
    B00001,
    B00000,
    B00010,
    B00010
  };
  
  lcd.createChar(1, fps);
  lcd.createChar(2, gun);
  lcd.createChar(3, reverseGun);
  lcd.createChar(4, bullet);
  lcd.createChar(5, reverseBullet);

  lcd.clear();
  lcd.print("\x04  ASG Chrono  \x05");
  lcd.setCursor(0, 1);
  lcd.print("\x02   by Spook   \x03");

  delay(2000);
  mainMenu.Show();
}

void loop()
{
  int key = keypad.ReadKey();
  int result = mainMenu.Action(key);
  if (result == 1)
  {
    fpsMeasurement();
    mainMenu.Show();
  }
}
