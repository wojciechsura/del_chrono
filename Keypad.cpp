#include "Keypad.h"
#include "Arduino.h"

Keypad::Keypad() 
{
  this->lastRead = millis();
  this->lastKey = KEY_NONE;
  this->repeats = 0;
}

void Keypad::setLastRead(int key) {

  if (key == lastKey)
  {
    if (repeats < REPEAT_COUNT2)
    repeats++;
  }
  else
    repeats = 0;

  lastKey = key;
}

// Button values (approximate):
// Right    0
// Up       144
// Down     329
// Left     504
// Select   741
unsigned char Keypad::ReadKey() 
{
  unsigned long now = millis();
  unsigned long t = now - this->lastRead;
  if (this->lastKey != KEY_NONE && 
    ((repeats < REPEAT_COUNT1 && t < LAST_READ_DELAY1) ||
     (repeats >= REPEAT_COUNT1 && repeats < REPEAT_COUNT2 && t < LAST_READ_DELAY2) ||
     (repeats >= REPEAT_COUNT2 && t < LAST_READ_DELAY3)) &&
     (now > this->lastRead))
  {
    return KEY_NONE;
  }
  
  int key = analogRead(keypadPort);      
  
  if (key > KEY_ANY_THRESHOLD)
    setLastRead(KEY_NONE);
  else if (key < KEY_RIGHT_THRESHOLD)
    setLastRead(KEY_RIGHT);
  else if (key < KEY_UP_THRESHOLD)
    setLastRead(KEY_UP);
  else if (key < KEY_DOWN_THRESHOLD)
    setLastRead(KEY_DOWN);
  else if (key < KEY_LEFT_THRESHOLD)
    setLastRead(KEY_LEFT);
  else if (key < KEY_SELECT_THRESHOLD)
    setLastRead(KEY_SELECT);
  else
    setLastRead(KEY_NONE);
  
  this->lastRead = now;

  return this->lastKey;
}

